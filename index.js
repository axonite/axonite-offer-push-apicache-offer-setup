'use strict';
var api = require('./api');
var axoniteModule = require('./axonite_module');
Object.keys(axoniteModule).forEach(function(key){
  api[key] = axoniteModule[key];
});
module.exports = api;
