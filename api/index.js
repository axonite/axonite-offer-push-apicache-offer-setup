'use strict';
var AxoniteModule = require('@axonite.io/axonite-core/lib/axonite_module');
var UniqueId      = require('@axonite.io/axonite-core/lib/unique_id_generator');
var promise       = require('bluebird');

module.exports = {
  initModule: function (opt) {
    return AxoniteModule.load(require('../axonite_module'), opt);
  },

  describeService: function(){
    var service = {
      model: 'offers',
      method: 'export'
    };
    service.naturalKey = [ service.method, service.model,"catalogServing-offer-setup"].join('-');
    service.id = UniqueId.naturalKeyToGuid(service.naturalKey);
    return service;
  },

  getServiceMeta: function(){
    return promise.resolve();
  }
};