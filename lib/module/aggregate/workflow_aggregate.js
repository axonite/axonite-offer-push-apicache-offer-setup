'use strict';
const CatalogChangefeedWorkflow  = require('@axonite.io/axonite-workflow/lib/module/aggregate/catalog_changefeed_workflow');
const logger = require('@axonite.io/axonite-core/lib/logger');
const Service = require('../../../api').describeService();
const CatalogServingModule = require('@axonite.io/axonite-catalog-serving');

class OfferPushApiCacheWorkflow extends CatalogChangefeedWorkflow {
  constructor(opt) {
    super(opt, {service: Service});
  }

  executeWorkflow(workflow) {
    return CatalogServingModule.executeSubscription(workflow.subscription)
      .then(summary => {
        logger.info(workflow.id, '[', workflow.service.naturalKey, '] executeWorkflowSummary', JSON.stringify(summary));
        workflow.cycle.lastExecution = summary;
      });
  }
}
  
  
module.exports = OfferPushApiCacheWorkflow;